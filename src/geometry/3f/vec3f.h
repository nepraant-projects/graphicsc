#ifndef __VEC3F__
#define __VEC3F__

typedef struct
{
  float x;
  float y;
  float z;
} vec3f_t;

vec3f_t vec3f_init(float x, float y, float z);

vec3f_t vec3f_add(vec3f_t v0, vec3f_t v1);

vec3f_t vec3f_sub(vec3f_t v0, vec3f_t v1);

vec3f_t vec3f_mul(vec3f_t v, float s);

float vec3f_dot(vec3f_t v0, vec3f_t v1);

vec3f_t vec3f_cross(vec3f_t v0, vec3f_t v1);

vec3f_t vec3f_rotate(vec3f_t v, float angle_rad, vec3f_t axis);

float vec3f_size(vec3f_t v);

vec3f_t vec3f_normalise(vec3f_t v);

void vec3f_print(vec3f_t v);

#endif
