#include "mat3f.h"
#include <math.h>
#include <stdio.h>

mat3f_t mat3f_init(float x00, float x01, float x02, float x10, float x11,
                   float x12, float x20, float x21, float x22)
{
  mat3f_t mat;
  mat.data[0][0] = x00;
  mat.data[0][1] = x01;
  mat.data[0][2] = x02;
  mat.data[1][0] = x10;
  mat.data[1][1] = x11;
  mat.data[1][2] = x12;
  mat.data[2][0] = x20;
  mat.data[2][1] = x21;
  mat.data[2][2] = x22;
  return mat;
}

mat3f_t mat3f_add(mat3f_t m0, mat3f_t m1)
{
  mat3f_t m;
  for (int i = 0; i < 3; ++i)
  {
    for (int j = 0; j < 3; ++j)
    {
      m.data[i][j] = m0.data[i][j] + m1.data[i][j];
    }
  }
  return m;
}

mat3f_t mat3f_sub(mat3f_t m0, mat3f_t m1)
{
  mat3f_t m;
  for (int i = 0; i < 3; ++i)
  {
    for (int j = 0; j < 3; ++j)
    {
      m.data[i][j] = m0.data[i][j] - m1.data[i][j];
    }
  }
  return m;
}

mat3f_t mat3f_mul_scal(mat3f_t m, float s)
{
  mat3f_t m_out;
  for (int i = 0; i < 3; ++i)
  {
    for (int j = 0; j < 3; ++j)
    {
      m_out.data[i][j] = m.data[i][j] * s;
    }
  }
  return m_out;
}

mat3f_t mat3f_mul_mat3f(mat3f_t m0, mat3f_t m1)
{
  mat3f_t m;
  for (int i = 0; i < 3; ++i)
  {
    for (int j = 0; j < 3; ++j)
    {
      m.data[i][j] = 0;
      for (int k = 0; k < 3; ++k)
      {
        m.data[i][j] += m0.data[i][k] * m1.data[k][j];
      }
    }
  }
  return m;
}

vec3f_t mat3f_mul_vec3f(mat3f_t m, vec3f_t v)
{
  vec3f_t v_out = vec3f_init(0, 0, 0);

  v_out.x += m.data[0][0] * v.x;
  v_out.x += m.data[0][1] * v.y;
  v_out.x += m.data[0][2] * v.z;

  v_out.y += m.data[1][0] * v.x;
  v_out.y += m.data[1][1] * v.y;
  v_out.y += m.data[1][2] * v.z;

  v_out.z += m.data[2][0] * v.x;
  v_out.z += m.data[2][1] * v.y;
  v_out.z += m.data[2][2] * v.z;

  return v_out;
}

mat3f_t mat3f_eye()
{
  return mat3f_init(1, 0, 0, 0, 1, 0, 0, 0, 1);
}

mat3f_t mat3f_zeros()
{
  return mat3f_init(0, 0, 0, 0, 0, 0, 0, 0, 0);
}

mat3f_t mat3f_rot_x(float angle_rad)
{
  float s = sinf(angle_rad);
  float c = cosf(angle_rad);
  return mat3f_init(1, 0, 0, 0, c, -s, 0, s, c);
}

mat3f_t mat3f_rot_y(float angle_rad)
{
  float s = sinf(angle_rad);
  float c = cosf(angle_rad);
  return mat3f_init(c, 0, s, 0, 1, 0, -s, 0, c);
}

mat3f_t mat3f_rot_z(float angle_rad)
{
  float s = sinf(angle_rad);
  float c = cosf(angle_rad);
  return mat3f_init(c, -s, 0, s, c, 0, 0, 0, 1);
}

mat3f_t mat3f_scale(float s)
{
  return mat3f_init(s, 0, 0, 0, s, 0, 0, 0, s);
}


void mat3f_print(mat3f_t m)
{
  printf("%lf %lf %lf\n", m.data[0][0], m.data[0][1], m.data[0][2]);
  printf("%lf %lf %lf\n", m.data[1][0], m.data[1][1], m.data[1][2]);
  printf("%lf %lf %lf\n", m.data[2][0], m.data[2][1], m.data[2][2]);
}
