#include "vec3f.h"
#include "../quaternion/quatf.h"
#include <math.h>
#include <stdio.h>

vec3f_t vec3f_init(float x, float y, float z)
{
  return (vec3f_t){x, y, z};
}

vec3f_t vec3f_add(vec3f_t v0, vec3f_t v1)
{
  return (vec3f_t){v0.x + v1.x, v0.y + v1.y, v0.z + v1.z};
}

vec3f_t vec3f_sub(vec3f_t v0, vec3f_t v1)
{
  return (vec3f_t){v0.x - v1.x, v0.y - v1.y, v0.z - v1.z};
}

vec3f_t vec3f_mul(vec3f_t v, float s)
{
  return (vec3f_t){v.x * s, v.y * s, v.z * s};
}

float vec3f_dot(vec3f_t v0, vec3f_t v1)
{
  return v0.x * v1.x + v0.y * v1.y + v0.z * v1.z;
}

vec3f_t vec3f_cross(vec3f_t v0, vec3f_t v1)
{
  vec3f_t res;
  res.x = v0.y * v1.z - v0.z * v1.y;
  res.y = v0.z * v1.x - v0.x * v1.z;
  res.z = v0.x * v1.y - v0.y * v1.x;
  return res;
}

vec3f_t vec3f_rotate(vec3f_t v, float angle_rad, vec3f_t axis)
{
  quatf_t rot_q = quatf_init(angle_rad, axis);

  quatf_t conj_q = quatf_conjugate(rot_q);

  quatf_t w = quatf_mul_quatf(quatf_mul_vec3f(rot_q, v), conj_q);

  return vec3f_init(w.x, w.y, w.z);
}

float vec3f_size(vec3f_t v)
{
  return sqrtf(vec3f_dot(v, v));
}

vec3f_t vec3f_normalise(vec3f_t v)
{
  return vec3f_mul(v, 1.0f / vec3f_size(v));
}

void vec3f_print(vec3f_t v)
{
  printf("%lf %lf %lf\n", v.x, v.y, v.z);
}
