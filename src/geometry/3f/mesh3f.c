#include "mesh3f.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define MESH3f_DEFAULT_TRIANGLE_CAP 16

mesh3f_t *mesh3f_create()
{
  mesh3f_t *mesh = (mesh3f_t *)malloc(sizeof(mesh3f_t));
  if (mesh == NULL)
  {
    return NULL;
  }

  mesh->triangles = (triangle3f_t *)malloc(sizeof(triangle3f_t) *
                                           MESH3f_DEFAULT_TRIANGLE_CAP);
  if (mesh->triangles == NULL)
  {
    free(mesh);
    return NULL;
  }

  mesh->normals =
      (vec3f_t *)malloc(sizeof(vec3f_t) * MESH3f_DEFAULT_TRIANGLE_CAP);
  if (mesh->normals == NULL)
  {
    free(mesh->triangles);
    free(mesh);
    return NULL;
  }

  mesh->triangle_cap = MESH3f_DEFAULT_TRIANGLE_CAP;
  mesh->triangle_cnt = 0;

  return mesh;
}

bool mesh3f_delete(mesh3f_t **mesh)
{
  if (mesh == NULL)
  {
    return false;
  }

  if (*mesh == NULL)
  {
    return true;
  }

  free((*mesh)->normals);
  free((*mesh)->triangles);
  free((*mesh));
  *mesh = NULL;

  return true;
}

bool _mesh3f_add_triangle_cap(mesh3f_t *mesh)
{
  triangle3f_t *tmp = (triangle3f_t *)realloc(
      mesh->triangles, sizeof(triangle3f_t) *
                           (mesh->triangle_cap + MESH3f_DEFAULT_TRIANGLE_CAP));
  if (tmp == NULL)
  {
    return false;
  }
  mesh->triangles = tmp;

  vec3f_t *tmp1 = (vec3f_t *)realloc(
      mesh->normals,
      sizeof(vec3f_t) * (mesh->triangle_cap + MESH3f_DEFAULT_TRIANGLE_CAP));
  if (tmp1 == NULL)
  {
    return false;
  }
  mesh->normals = tmp1;
  mesh->triangle_cap += MESH3f_DEFAULT_TRIANGLE_CAP;

  return true;
}

bool mesh3f_add_triangle(mesh3f_t *mesh, triangle3f_t t, vec3f_t n)
{
  if (mesh == NULL)
  {
    return false;
  }

  if (mesh->triangle_cap == mesh->triangle_cnt &&
      !_mesh3f_add_triangle_cap(mesh))
  {
    return false;
  }

  mesh->triangles[mesh->triangle_cnt] = t;
  mesh->normals[mesh->triangle_cnt] = n;
  ++mesh->triangle_cnt;

  return true;
}

mesh3f_t *mesh3f_load_stl_bin(const char *filename)
{
  FILE *file = fopen(filename, "r");
  if (file == NULL)
  {
    return false;
  }

  uint8_t header[80];
  if (fread(header, sizeof(uint8_t), 80, file) != 80)
  {
    fclose(file);
    return NULL;
  }

  uint32_t triangle_num;
  if (fread(&triangle_num, sizeof(uint32_t), 1, file) != 1)
  {
    fclose(file);
    return NULL;
  }

  mesh3f_t *mesh = mesh3f_create();
  if (mesh == NULL)
  {
    fclose(file);
    return NULL;
  }

  for (uint32_t i = 0; i < triangle_num; ++i)
  {
    vec3f_t n;
    fread(&n, 3, sizeof(float), file);

    float v0[3];
    if (fread(v0, sizeof(float), 3, file) != 3)
    {
      fclose(file);
      mesh3f_delete(&mesh);
      return NULL;
    }

    float v1[3];
    if (fread(v1, sizeof(float), 3, file) != 3)
    {
      fclose(file);
      mesh3f_delete(&mesh);
      return NULL;
    }

    float v2[3];
    if (fread(v2, sizeof(float), 3, file) != 3)
    {
      fclose(file);
      mesh3f_delete(&mesh);
      return NULL;
    }

    // skip attribute
    fseek(file, sizeof(uint16_t), SEEK_CUR);

    triangle3f_t t = triangle3f_init(vec3f_init(v0[0], v0[1], v0[2]),
                                     vec3f_init(v1[0], v1[1], v1[2]),
                                     vec3f_init(v2[0], v2[1], v2[2]));

    if (!mesh3f_add_triangle(mesh, t, n))
    {
      fclose(file);
      mesh3f_delete(&mesh);
      return NULL;
    }
  }

  fclose(file);
  return mesh;
}

bool mesh3f_scale(mesh3f_t *mesh, float s)
{
  if (mesh == NULL)
  {
    return false;
  }

  for (int i = 0; i < mesh->triangle_cnt; ++i)
  {
    for (int j = 0; j < 3; ++j)
    {
      mesh->triangles[i].vertices[j] =
          vec3f_mul(mesh->triangles[i].vertices[j], s);
    }
  }

  return true;
}
