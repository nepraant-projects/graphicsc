#ifndef __MAT3F__
#define __MAT3F__

#include "vec3f.h"

typedef struct
{
  float data[3][3];
} mat3f_t;

mat3f_t mat3f_init(float x00, float x01, float x02, float x10, float x11,
                   float x12, float x20, float x21, float x22);

mat3f_t mat3f_add(mat3f_t m0, mat3f_t m1);

mat3f_t mat3f_sub(mat3f_t m0, mat3f_t m1);

mat3f_t mat3f_mul_scal(mat3f_t m, float s);

mat3f_t mat3f_mul_mat3f(mat3f_t m0, mat3f_t m1);

vec3f_t mat3f_mul_vec3f(mat3f_t m, vec3f_t v);

mat3f_t mat3f_eye();

mat3f_t mat3f_zeros();

mat3f_t mat3f_rot_x(float angle_rad);

mat3f_t mat3f_rot_y(float angle_rad);

mat3f_t mat3f_rot_z(float angle_rad);

mat3f_t mat3f_scale(float s);

void mat3f_print(mat3f_t m);

#endif
