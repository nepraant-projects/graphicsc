#ifndef __MESH3F__
#define __MESH3F__

#include "triangle3f.h"
#include <stdbool.h>

typedef struct
{
  triangle3f_t *triangles;
  vec3f_t *normals;
  unsigned triangle_cnt;
  unsigned triangle_cap;
} mesh3f_t;

mesh3f_t *mesh3f_create();

bool mesh3f_delete(mesh3f_t **mesh);

bool mesh3f_add_triangle(mesh3f_t *mesh, triangle3f_t t, vec3f_t n);

mesh3f_t *mesh3f_load_stl_bin(const char *filename);

bool mesh3f_scale(mesh3f_t *mesh, float s);

#endif
