#ifndef __TRIANGLE3F__
#define __TRIANGLE3F__

#include "vec3f.h"

typedef struct
{
  vec3f_t vertices[3];
} triangle3f_t;

triangle3f_t triangle3f_init(vec3f_t v0, vec3f_t v1, vec3f_t v2);

void triangle3f_print(triangle3f_t t);

#endif
