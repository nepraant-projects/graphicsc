#include "mat4f.h"
#include <math.h>
#include <stdio.h>

mat4f_t mat4f_init(float x00, float x01, float x02, float x03, float x10,
                   float x11, float x12, float x13, float x20, float x21,
                   float x22, float x23, float x30, float x31, float x32,
                   float x33)
{
  mat4f_t mat;
  mat.data[0][0] = x00;
  mat.data[0][1] = x01;
  mat.data[0][2] = x02;
  mat.data[0][3] = x03;
  mat.data[1][0] = x10;
  mat.data[1][1] = x11;
  mat.data[1][2] = x12;
  mat.data[1][3] = x13;
  mat.data[2][0] = x20;
  mat.data[2][1] = x21;
  mat.data[2][2] = x22;
  mat.data[2][3] = x23;
  mat.data[3][0] = x30;
  mat.data[3][1] = x31;
  mat.data[3][2] = x32;
  mat.data[3][3] = x33;
  return mat;
}

mat4f_t mat4f_add(mat4f_t m0, mat4f_t m1)
{
  mat4f_t m;
  for (int i = 0; i < 4; ++i)
  {
    for (int j = 0; j < 4; ++j)
    {
      m.data[i][j] = m0.data[i][j] + m1.data[i][j];
    }
  }
  return m;
}

mat4f_t mat4f_sub(mat4f_t m0, mat4f_t m1)
{
  mat4f_t m;
  for (int i = 0; i < 4; ++i)
  {
    for (int j = 0; j < 4; ++j)
    {
      m.data[i][j] = m0.data[i][j] - m1.data[i][j];
    }
  }
  return m;
}

mat4f_t mat4f_mul_scal(mat4f_t m, float s)
{
  mat4f_t m_out;
  for (int i = 0; i < 4; ++i)
  {
    for (int j = 0; j < 4; ++j)
    {
      m_out.data[i][j] = m.data[i][j] * s;
    }
  }
  return m_out;
}

mat4f_t mat4f_mul_mat4f(mat4f_t m0, mat4f_t m1)
{
  mat4f_t m;
  for (int i = 0; i < 4; ++i)
  {
    for (int j = 0; j < 4; ++j)
    {
      m.data[i][j] = 0;
      for (int k = 0; k < 4; ++k)
      {
        m.data[i][j] += m0.data[i][k] * m1.data[k][j];
      }
    }
  }
  return m;
}

vec4f_t mat4f_mul_vec4f(mat4f_t m, vec4f_t v)
{
  vec4f_t v_out = vec4f_init(0, 0, 0, 0);

  v_out.x += m.data[0][0] * v.x;
  v_out.x += m.data[0][1] * v.y;
  v_out.x += m.data[0][2] * v.z;
  v_out.x += m.data[0][3] * v.w;

  v_out.y += m.data[1][0] * v.x;
  v_out.y += m.data[1][1] * v.y;
  v_out.y += m.data[1][2] * v.z;
  v_out.y += m.data[1][3] * v.w;

  v_out.z += m.data[2][0] * v.x;
  v_out.z += m.data[2][1] * v.y;
  v_out.z += m.data[2][2] * v.z;
  v_out.z += m.data[2][3] * v.w;

  v_out.w += m.data[3][0] * v.x;
  v_out.w += m.data[3][1] * v.y;
  v_out.w += m.data[3][2] * v.z;
  v_out.w += m.data[3][3] * v.w;

  return v_out;
}

mat4f_t mat4f_eye()
{
  return mat4f_init(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
}

mat4f_t mat4f_zeros()
{
  return mat4f_init(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
}

mat4f_t mat4f_rot_x(float angle_rad)
{
  float s = sinf(angle_rad);
  float c = cosf(angle_rad);
  return mat4f_init(1, 0, 0, 0, 0, c, -s, 0, 0, s, c, 0, 0, 0, 0, 1);
}

mat4f_t mat4f_rot_y(float angle_rad)
{
  float s = sinf(angle_rad);
  float c = cosf(angle_rad);
  return mat4f_init(c, 0, s, 0, 0, 1, 0, 0, -s, 0, c, 0, 0, 0, 0, 1);
}

mat4f_t mat4f_rot_z(float angle_rad)
{
  float s = sinf(angle_rad);
  float c = cosf(angle_rad);
  return mat4f_init(c, -s, 0, 0, s, c, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
}

mat4f_t mat4f_rot_xyz(float angle_rad_x, float angle_rad_y, float angle_rad_z)
{
  mat4f_t rot_x = mat4f_rot_x(angle_rad_x);
  mat4f_t rot_y = mat4f_rot_y(angle_rad_y);
  mat4f_t rot_z = mat4f_rot_z(angle_rad_z);

  return mat4f_mul_mat4f(rot_z, mat4f_mul_mat4f(rot_y, rot_x));
}

mat4f_t mat4f_translate_x(float x)
{
  return mat4f_init(1, 0, 0, x, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
}

mat4f_t mat4f_translate_y(float y)
{
  return mat4f_init(1, 0, 0, 0, 0, 1, 0, y, 0, 0, 1, 0, 0, 0, 0, 1);
}

mat4f_t mat4f_translate_z(float z)
{
  return mat4f_init(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, z, 0, 0, 0, 1);
}

mat4f_t mat4f_translate_xyz(float x, float y, float z)
{
  return mat4f_init(1, 0, 0, x, 0, 1, 0, y, 0, 0, 1, z, 0, 0, 0, 1);
}

mat4f_t mat4f_scale(float s)
{
  return mat4f_init(s, 0, 0, 0, 0, s, 0, 0, 0, 0, s, 0, 0, 0, 0, 1);
}

mat4f_t mat4f_perspective(float fov, float near, float far, float ar)
{
  float d = 1.0 / tanf(fov / 2.0);
  float z_range = near - far;
  float A = (-far - near) / z_range;
  float B = 2.0f * far * near / z_range;
  return mat4f_init(d / ar, 0, 0, 0, 0, d, 0, 0, 0, 0, A, B, 0, 0, 1, 0);
}

void mat4f_print(mat4f_t m)
{
  printf("%lf %lf %lf %lf\n", m.data[0][0], m.data[0][1], m.data[0][2],
         m.data[0][3]);
  printf("%lf %lf %lf %lf\n", m.data[1][0], m.data[1][1], m.data[1][2],
         m.data[1][3]);
  printf("%lf %lf %lf %lf\n", m.data[2][0], m.data[2][1], m.data[2][2],
         m.data[2][3]);
  printf("%lf %lf %lf %lf\n", m.data[3][0], m.data[3][1], m.data[3][2],
         m.data[3][3]);
}
