#ifndef __VEC4F__
#define __VEC4F__

typedef struct
{
  float x;
  float y;
  float z;
  float w;
} vec4f_t;

vec4f_t vec4f_init(float x, float y, float z, float w);

vec4f_t vec4f_add(vec4f_t v0, vec4f_t v1);

vec4f_t vec4f_sub(vec4f_t v0, vec4f_t v1);

vec4f_t vec4f_mul(vec4f_t v, float s);

float vec4f_dot(vec4f_t v0, vec4f_t v1);

void vec4f_print(vec4f_t v);

#endif
