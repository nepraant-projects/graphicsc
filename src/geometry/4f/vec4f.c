#include "vec4f.h"
#include <stdio.h>

vec4f_t vec4f_init(float x, float y, float z, float w)
{
  return (vec4f_t){x, y, z, w};
}

vec4f_t vec4f_add(vec4f_t v0, vec4f_t v1)
{
  return (vec4f_t){v0.x + v1.x, v0.y + v1.y, v0.z + v1.z, v0.w + v1.w};
}

vec4f_t vec4f_sub(vec4f_t v0, vec4f_t v1)
{
  return (vec4f_t){v0.x - v1.x, v0.y - v1.y, v0.z - v1.z, v0.w - v1.w};
}

vec4f_t vec4f_mul(vec4f_t v, float s)
{
  return (vec4f_t){v.x * s, v.y * s, v.z * s, v.w * s};
}

float vec4f_dot(vec4f_t v0, vec4f_t v1)
{
  return v0.x * v1.x + v0.y * v1.y + v0.z * v1.z + v0.w * v1.w;
}

void vec4f_print(vec4f_t v)
{
  printf("%lf %lf %lf %lf\n", v.x, v.y, v.z, v.w);
}
