#ifndef __QUATF__
#define __QUATF__

#include "../3f/vec3f.h"

typedef struct
{
  float x;
  float y;
  float z;
  float w;
} quatf_t;

quatf_t quatf_init(float angle, vec3f_t v);

quatf_t quatf_normalise(quatf_t q);

quatf_t quatf_conjugate(quatf_t q);

quatf_t quatf_mul_vec3f(quatf_t q, vec3f_t v);

quatf_t quatf_mul_quatf(quatf_t q0, quatf_t q1);

#endif
