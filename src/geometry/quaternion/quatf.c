#include "quatf.h"
#include <math.h>

quatf_t quatf_init(float angle, vec3f_t v)
{
  float sin_hanf_angle = sinf(angle / 2);
  float cos_hanf_angle = cosf(angle / 2);

  quatf_t q;
  q.x = v.x * sin_hanf_angle;
  q.y = v.y * sin_hanf_angle;
  q.z = v.z * sin_hanf_angle;
  q.w = cos_hanf_angle;

  return q;
}

quatf_t quatf_normalise(quatf_t q)
{
  float length = sqrtf(q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w);

  quatf_t q_norm = q;
  q_norm.x /= length;
  q_norm.y /= length;
  q_norm.z /= length;
  q_norm.w /= length;
  return q_norm;
}

quatf_t quatf_conjugate(quatf_t q)
{
  return (quatf_t){-q.x, -q.y, -q.z, q.w};
}

quatf_t quatf_mul_vec3f(quatf_t q, vec3f_t v)
{
  quatf_t res;
  res.w = -(q.x * v.x) - (q.y * v.y) - (q.z * v.z);
  res.x = (q.w * v.x) + (q.y * v.z) - (q.z * v.y);
  res.y = (q.w * v.y) + (q.z * v.x) - (q.x * v.z);
  res.z = (q.w * v.z) + (q.x * v.y) - (q.y * v.x);
  return res;
}

quatf_t quatf_mul_quatf(quatf_t q0, quatf_t q1)
{
  quatf_t res;
  res.w = (q0.w * q1.w) - (q0.x * q1.x) - (q0.y * q1.y) - (q0.z * q1.z);
  res.x = (q0.x * q1.w) + (q0.w * q1.x) + (q0.y * q1.z) - (q0.z * q1.y);
  res.y = (q0.y * q1.w) + (q0.w * q1.y) + (q0.z * q1.x) - (q0.x * q1.z);
  res.z = (q0.z * q1.w) + (q0.w * q1.z) + (q0.x * q1.y) - (q0.y * q1.x);
  return res;
}
