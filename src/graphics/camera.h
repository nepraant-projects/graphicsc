#ifndef __CAMERA__
#define __CAMERA__

#include "../geometry/geometry.h"

typedef struct
{
  vec3f_t pos;
  vec3f_t target;
  vec3f_t up;

  float speed;

  int window_w;
  int window_h;

  float angle_h;
  float angle_v;

  bool on_upper_edge;
  bool on_lower_edge;
  bool on_left_edge;
  bool on_right_edge;

  int mouse_x;
  int mouse_y;
} camera_t;

camera_t *camera_create(vec3f_t pos, vec3f_t target, vec3f_t up, int window_w,
                        int window_h);

bool camera_delete(camera_t **cam);

mat4f_t camera_get_matrix(camera_t *cam);

void camera_on_keyboard(camera_t *cam, unsigned char key);

void camera_on_mouse(camera_t *cam, int x, int y);

void camera_on_render(camera_t *cam);


#endif
