#include "world_transf.h"


world_transf_t world_tranf_init(vec3f_t pos, vec3f_t rot, float scale)
{
  return (world_transf_t){pos, rot, scale};
}

mat4f_t world_transf_get_matrix(world_transf_t transf)
{
  mat4f_t scale = mat4f_scale(transf.scale);
  mat4f_t rotation = mat4f_rot_xyz(transf.rot.x, transf.rot.y, transf.rot.z);
  mat4f_t translation =
      mat4f_translate_xyz(transf.pos.x, transf.pos.y, transf.pos.z);

  return mat4f_mul_mat4f(translation, mat4f_mul_mat4f(rotation, scale));
}
