#ifndef __OBJECT_BUFFER__
#define __OBJECT_BUFFER__

#include "../geometry/geometry.h"

typedef struct
{
  vec3f_t *data;
  unsigned size;
} object_buffer_t;

object_buffer_t *object_buffer_create();

bool object_buffer_delete(object_buffer_t **buffer);

bool object_buffer_add_mesh_color(object_buffer_t *buffer, mesh3f_t *mesh,
                                  vec3f_t color, unsigned *buffer_position,
                                  unsigned *size);

bool object_buffer_add_mesh_colors(object_buffer_t *buffer, mesh3f_t *mesh,
                                   vec3f_t *colors,
                                   unsigned int *buffer_position,
                                   unsigned int *size);

#endif
