#include "verlet3f_body_primitives.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

verlet3f_body_t *verlet3f_body_primitive_cuboid(float verlet_radius,
                                                float verlet_weight,
                                                vec3f_t position, float w,
                                                float h, float d)
{
  verlet3f_body_t *body = verlet3f_body_create();
  if (body == NULL)
  {
    return NULL;
  }

  verlet3f_body_add_verlet(
      body, verlet3f_create(position, verlet_weight, verlet_radius));
  verlet3f_body_add_verlet(
      body, verlet3f_create(vec3f_add(position, vec3f_init(w, 0, 0)),
                            verlet_weight, verlet_radius));
  verlet3f_body_add_verlet(
      body, verlet3f_create(vec3f_add(position, vec3f_init(w, h, 0)),
                            verlet_weight, verlet_radius));
  verlet3f_body_add_verlet(
      body, verlet3f_create(vec3f_add(position, vec3f_init(0, h, 0)),
                            verlet_weight, verlet_radius));
  verlet3f_body_add_verlet(
      body, verlet3f_create(vec3f_add(position, vec3f_init(0, 0, d)),
                            verlet_weight, verlet_radius));
  verlet3f_body_add_verlet(
      body, verlet3f_create(vec3f_add(position, vec3f_init(w, 0, d)),
                            verlet_weight, verlet_radius));
  verlet3f_body_add_verlet(
      body, verlet3f_create(vec3f_add(position, vec3f_init(w, h, d)),
                            verlet_weight, verlet_radius));
  verlet3f_body_add_verlet(
      body, verlet3f_create(vec3f_add(position, vec3f_init(0, h, d)),
                            verlet_weight, verlet_radius));

  verlet3f_body_add_all_dist_constrains(body);

  return body;
}

verlet3f_body_t *verlet3f_body_primitive_chain(float verlet_radius,
                                               float verlet_weight, vec3f_t v0,
                                               vec3f_t v1, unsigned n)
{
  verlet3f_body_t *body = verlet3f_body_create();
  if (body == NULL || n == 0)
  {
    return NULL;
  }

  vec3f_t cur_pos = v0;
  vec3f_t iter_vec = vec3f_mul(vec3f_sub(v1, v0), 1.0 / (n - 1));
  verlet3f_body_add_verlet(
      body, verlet3f_create(cur_pos, verlet_weight, verlet_radius));
  for (unsigned i = 1; i < n; ++i)
  {
    cur_pos = vec3f_add(cur_pos, iter_vec);
    verlet3f_body_add_verlet(
        body, verlet3f_create(cur_pos, verlet_weight, verlet_radius));

    verlet3f_body_add_dist_constrain(body, i - 1, i);
  }

  return body;
}

verlet3f_body_t *verlet3f_body_primitive_tetrahedron(float verlet_radius,
                                                     float verlet_weight,
                                                     vec3f_t position,
                                                     float size)
{
  verlet3f_body_t *body = verlet3f_body_create();
  if (body == NULL)
  {
    return NULL;
  }

  vec3f_t v0 = vec3f_add(
      position,
      vec3f_mul(vec3f_init(sqrtf(8.0f / 9.0f), -1.0f / 3.0f, 0), size));
  vec3f_t v1 = vec3f_add(position,
                         vec3f_mul(vec3f_init(-sqrtf(2.0f / 9.0f), -1.0f / 3.0f,
                                              sqrtf(2.0f / 3.0f)),
                                   size));
  vec3f_t v2 = vec3f_add(position,
                         vec3f_mul(vec3f_init(-sqrtf(2.0f / 9.0f), -1.0f / 3.0f,
                                              -sqrtf(2.0f / 3.0f)),
                                   size));
  vec3f_t v3 = vec3f_add(position, vec3f_mul(vec3f_init(0, 1, 0), size));

  verlet3f_body_add_verlet(body,
                           verlet3f_create(v0, verlet_weight, verlet_radius));

  verlet3f_body_add_verlet(body,
                           verlet3f_create(v1, verlet_weight, verlet_radius));

  verlet3f_body_add_verlet(body,
                           verlet3f_create(v2, verlet_weight, verlet_radius));

  verlet3f_body_add_verlet(body,
                           verlet3f_create(v3, verlet_weight, verlet_radius));

  verlet3f_body_add_all_dist_constrains(body);

  return body;
}

verlet3f_body_t *verlet3f_body_primitive_pyramid(float verlet_radius,
                                                 float verlet_weight,
                                                 vec3f_t position, float size,
                                                 float heigth)
{
  verlet3f_body_t *body = verlet3f_body_create();
  if (body == NULL)
  {
    return NULL;
  }

  vec3f_t v0 = position;
  vec3f_t v1 = vec3f_add(position, vec3f_init(size, 0, 0));
  vec3f_t v2 = vec3f_add(position, vec3f_init(0, 0, size));
  vec3f_t v3 = vec3f_add(position, vec3f_init(size, 0, size));
  vec3f_t v4 = vec3f_add(position, vec3f_init(size / 2, heigth, size / 2));

  verlet3f_body_add_verlet(body,
                           verlet3f_create(v0, verlet_weight, verlet_radius));
  verlet3f_body_add_verlet(body,
                           verlet3f_create(v1, verlet_weight, verlet_radius));
  verlet3f_body_add_verlet(body,
                           verlet3f_create(v2, verlet_weight, verlet_radius));
  verlet3f_body_add_verlet(body,
                           verlet3f_create(v3, verlet_weight, verlet_radius));
  verlet3f_body_add_verlet(body,
                           verlet3f_create(v4, verlet_weight, verlet_radius));

  verlet3f_body_add_all_dist_constrains(body);

  return body;
}

verlet3f_body_t *verlet3f_body_primitive_sphere(float verlet_radius,
                                                float verlet_weight,
                                                vec3f_t position, float radius,
                                                unsigned v_divs,
                                                unsigned r_divs)
{
  verlet3f_body_t *body = verlet3f_body_create();
  if (body == NULL)
  {
    return NULL;
  }

  verlet3f_body_add_verlet(
      body, verlet3f_create(vec3f_add(position, vec3f_init(0, 0, radius)),
                            verlet_weight, verlet_radius));

  verlet3f_body_add_verlet(
      body, verlet3f_create(vec3f_add(position, vec3f_init(0, 0, -radius)),
                            verlet_weight, verlet_radius));

  for (unsigned i = 0; i < v_divs; ++i)
  {
    float v_angle = (i + 1) * M_PI / (v_divs + 1);
    float sphere_z = radius * cosf(v_angle);

    for (unsigned j = 0; j < r_divs; ++j)
    {
      float r_angle = j * 2 * M_PI / r_divs;
      float sphere_x = radius * sinf(r_angle) * sin(v_angle);
      float sphere_y = radius * cosf(r_angle) * sin(v_angle);


      vec3f_t v = vec3f_init(sphere_x, sphere_y, sphere_z);

      verlet3f_body_add_verlet(body,
                               verlet3f_create(vec3f_add(position, v),
                                               verlet_weight, verlet_radius));
    }
  }

  verlet3f_body_add_all_dist_constrains(body);


  return body;
}

verlet3f_body_t *verlet3f_body_primitive_fixed_chain(float verlet_radius,
                                                     float verlet_weight,
                                                     vec3f_t v0, vec3f_t v1,
                                                     unsigned n, bool fixed0,
                                                     bool fixed1)
{
  verlet3f_body_t *body =
      verlet3f_body_primitive_chain(verlet_radius, verlet_weight, v0, v1, n);
  if (body == NULL)
  {
    return NULL;
  }

  if (fixed0)
  {
    verlet3f_body_add_pos_constrain(body, 0, v0);
  }

  if (fixed1)
  {
    verlet3f_body_add_pos_constrain(body, body->verlet_cnt - 1, v1);
  }

  return body;
}

verlet3f_body_t *verlet3f_body_primitive_net(float verlet_radius,
                                             float verlet_weight, vec3f_t pos,
                                             vec3f_t size0, vec3f_t size1,
                                             unsigned n0, unsigned n1)
{
  verlet3f_body_t *body = verlet3f_body_create();
  if (body == NULL || n0 == 0 || n1 == 0)
  {
    return NULL;
  }

  vec3f_t iter0 = vec3f_mul(size0, 1.0f / n0);
  vec3f_t iter1 = vec3f_mul(size1, 1.0f / n1);

  for (unsigned i = 0; i < n0; ++i)
  {
    for (unsigned j = 0; j < n1; ++j)
    {
      vec3f_t v =
          vec3f_add(pos, vec3f_add(vec3f_mul(iter0, i), vec3f_mul(iter1, j)));
      verlet3f_body_add_verlet(
          body, verlet3f_create(v, verlet_weight, verlet_radius));
    }
  }

  for (unsigned i = 0; i < n0 - 1; ++i)
  {
    for (unsigned j = 0; j < n1; ++j)
    {
      verlet3f_body_add_dist_constrain(body, i * n1 + j, (i + 1) * n1 + j);
    }
  }

  for (unsigned i = 0; i < n1 - 1; ++i)
  {
    for (unsigned j = 0; j < n0; ++j)
    {
      verlet3f_body_add_dist_constrain(body, j * n1 + i, j * n1 + (i + 1));
    }
  }

  return body;
}

verlet3f_body_t *
verlet3f_body_primitive_fixed_net(float verlet_radius, float verlet_weight,
                                  vec3f_t pos, vec3f_t size0, vec3f_t size1,
                                  unsigned n0, unsigned n1, bool fixed00,
                                  bool fixed01, bool fixed10, bool fixed11)
{
  verlet3f_body_t *body = verlet3f_body_primitive_net(
      verlet_radius, verlet_weight, pos, size0, size1, n0, n1);
  if (body == NULL)
  {
    return NULL;
  }

  if (fixed00)
    verlet3f_body_add_pos_constrain(body, 0, pos);
  if (fixed01)
    verlet3f_body_add_pos_constrain(body, n0 - 1, vec3f_add(pos, size0));
  if (fixed10)
    verlet3f_body_add_pos_constrain(body, n1 * (n0 - 1), vec3f_add(pos, size1));
  if (fixed11)
    verlet3f_body_add_pos_constrain(body, n0 * n1 - 1,
                                    vec3f_add(pos, vec3f_add(size0, size1)));

  return body;
}


verlet3f_body_t *verlet3f_body_primitive_fixed_sides_net(
    float verlet_radius, float verlet_weight, vec3f_t pos, vec3f_t size0,
    vec3f_t size1, unsigned n0, unsigned n1)
{
  verlet3f_body_t *body = verlet3f_body_primitive_net(
      verlet_radius, verlet_weight, pos, size0, size1, n0, n1);
  if (body == NULL)
  {
    return NULL;
  }

  for (unsigned i = 0; i < n1; ++i)
  {
    verlet3f_body_add_pos_constrain(body, i, body->verlets[i]->pos);
    verlet3f_body_add_pos_constrain(body, n1 * (n0 - 1) + i,
                                    body->verlets[n1 * (n0 - 1) + i]->pos);
  }

  for (unsigned i = 1; i < n0 - 1; ++i)
  {
    verlet3f_body_add_pos_constrain(body, i * n1, body->verlets[i * n1]->pos);
    verlet3f_body_add_pos_constrain(body, i * n1 + n1 - 1,
                                    body->verlets[i * n1 + n1 - 1]->pos);
  }


  return body;
}
