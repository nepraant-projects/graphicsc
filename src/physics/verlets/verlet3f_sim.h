#ifndef __VERLET3F_SIM__
#define __VERLET3F_SIM__

#include "verlet3f.h"
#include "verlet3f_body.h"

typedef struct
{
  verlet3f_body_t **bodies;
  unsigned body_cap;
  unsigned body_cnt;
} verlet3f_sim_t;

verlet3f_sim_t *verlet3f_sim_create();

bool verlet3f_sim_delete(verlet3f_sim_t **sim);

bool verlet3f_sim_add(verlet3f_sim_t *sim, verlet3f_body_t *body);

bool verlet3f_sim_remove(verlet3f_sim_t *sim, unsigned index);

bool verlet3f_sim_move(verlet3f_sim_t *sim, float dt);

bool verlet3f_sim_resolve_body_constrains(verlet3f_sim_t *sim);

bool verlet3f_sim_resolve_collisions(verlet3f_sim_t *sim);

bool verlet3f_sim_constrain_x_max(verlet3f_sim_t *sim, float x);

bool verlet3f_sim_constrain_x_min(verlet3f_sim_t *sim, float x);

bool verlet3f_sim_constrain_y_max(verlet3f_sim_t *sim, float y);

bool verlet3f_sim_constrain_y_min(verlet3f_sim_t *sim, float y);

bool verlet3f_sim_constrain_z_max(verlet3f_sim_t *sim, float z);

bool verlet3f_sim_constrain_z_min(verlet3f_sim_t *sim, float z);

bool verlet3f_sim_constrain_in_sphere(verlet3f_sim_t *sim,
                                      vec3f_t sphere_position,
                                      float sphere_radius);

bool verlet3f_sim_constrain_out_sphere(verlet3f_sim_t *sim,
                                       vec3f_t sphere_position,
                                       float sphere_radius);

bool verlet3f_sim_set_acceleration(verlet3f_sim_t *sim, vec3f_t acc);

bool verlet3f_sim_apply_force(verlet3f_sim_t *sim, vec3f_t force);

bool verlet3f_sim_apply_force_in_area(verlet3f_sim_t *sim, float x_min,
                                      float x_max, float y_min, float y_max,
                                      float z_min, float z_max, vec3f_t force);

bool verlet3f_sim_apply_resistance(verlet3f_sim_t *sim, float amount);

#endif
