#include "verlet3f_sim.h"
#include <math.h>
#include <stdlib.h>

#define VERLET3F_SIM_DEFAULT_VERLET_CAP 16

verlet3f_sim_t *verlet3f_sim_create()
{
  verlet3f_sim_t *sim = (verlet3f_sim_t *)malloc(sizeof(verlet3f_sim_t));
  if (sim == NULL)
  {
    return NULL;
  }

  sim->bodies = (verlet3f_body_t **)malloc(sizeof(verlet3f_body_t *) *
                                           VERLET3F_SIM_DEFAULT_VERLET_CAP);
  if (sim->bodies == NULL)
  {
    free(sim);
    return NULL;
  }

  sim->body_cap = VERLET3F_SIM_DEFAULT_VERLET_CAP;
  sim->body_cnt = 0;

  return sim;
}

bool verlet3f_sim_delete(verlet3f_sim_t **sim)
{
  if (sim == NULL)
  {
    return false;
  }

  if (*sim == NULL)
  {
    return true;
  }

  for (unsigned i = 0; i < (*sim)->body_cnt; ++i)
  {
    verlet3f_body_delete(&(*sim)->bodies[i]);
  }

  free((*sim)->bodies);
  free(*sim);
  *sim = NULL;

  return true;
}

bool _verlet3f_sim_add_body_cap(verlet3f_sim_t *sim)
{
  if (sim == NULL)
  {
    return false;
  }

  verlet3f_body_t **tmp = (verlet3f_body_t **)realloc(
      sim->bodies, sizeof(verlet3f_body_t *) *
                       (sim->body_cap + VERLET3F_SIM_DEFAULT_VERLET_CAP));
  if (tmp == NULL)
  {
    return false;
  }

  sim->bodies = tmp;
  sim->body_cap += VERLET3F_SIM_DEFAULT_VERLET_CAP;

  return true;
}

bool verlet3f_sim_add(verlet3f_sim_t *sim, verlet3f_body_t *body)
{
  if (sim == NULL || body == NULL)
  {
    return false;
  }

  if (sim->body_cap == sim->body_cnt && !_verlet3f_sim_add_body_cap(sim))
  {
    return false;
  }

  sim->bodies[sim->body_cnt] = body;
  ++sim->body_cnt;

  return true;
}

bool verlet3f_sim_remove(verlet3f_sim_t *sim, unsigned index)
{
  if (sim == NULL || index >= sim->body_cnt)
  {
    return false;
  }

  verlet3f_body_delete(&sim->bodies[index]);
  for (unsigned i = index + 1; i < sim->body_cnt; ++i)
  {
    sim->bodies[i - 1] = sim->bodies[i];
  }

  --sim->body_cnt;

  return true;
}

bool verlet3f_sim_move(verlet3f_sim_t *sim, float dt)
{
  {
    if (sim == NULL)
    {
      return false;
    }

    for (unsigned i = 0; i < sim->body_cnt; ++i)
    {
      verlet3f_body_move(sim->bodies[i], dt);
    }

    return true;
  }
}

bool verlet3f_sim_resolve_body_constrains(verlet3f_sim_t *sim)
{
  {
    if (sim == NULL)
    {
      return false;
    }

    for (unsigned i = 0; i < sim->body_cnt; ++i)
    {
      verlet3f_body_resolve_constrains(sim->bodies[i]);
    }

    return true;
  }
}


bool verlet3f_sim_resolve_collisions(verlet3f_sim_t *sim)
{
  if (sim == NULL)
  {
    return false;
  }

  vec3f_t bb_positions[sim->body_cnt];
  vec3f_t bb_sizes[sim->body_cnt];

  for (unsigned i = 0; i < sim->body_cnt; ++i)
  {
    verlet3f_body_bounding_box(sim->bodies[i], bb_positions + i, bb_sizes + i);
  }


  for (unsigned i = 0; i < sim->body_cnt; ++i)
  {
    for (unsigned j = 0; j < i; ++j)
    {
      if (!verlet3f_body_bounding_boxex_collide(bb_positions[i], bb_sizes[i],
                                                bb_positions[j], bb_sizes[j]))
      {
        continue;
      }

      verlet3f_body_resolve_collisions(sim->bodies[i], sim->bodies[j]);
    }
  }

  return true;
}

bool verlet3f_sim_constrain_x_max(verlet3f_sim_t *sim, float x)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->body_cnt; ++i)
  {
    verlet3f_body_constrain_x_max(sim->bodies[i], x);
  }

  return true;
}

bool verlet3f_sim_constrain_x_min(verlet3f_sim_t *sim, float x)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->body_cnt; ++i)
  {
    verlet3f_body_constrain_x_min(sim->bodies[i], x);
  }

  return true;
}

bool verlet3f_sim_constrain_y_max(verlet3f_sim_t *sim, float y)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->body_cnt; ++i)
  {
    verlet3f_body_constrain_y_max(sim->bodies[i], y);
  }

  return true;
}

bool verlet3f_sim_constrain_y_min(verlet3f_sim_t *sim, float y)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->body_cnt; ++i)
  {
    verlet3f_body_constrain_y_min(sim->bodies[i], y);
  }

  return true;
}

bool verlet3f_sim_constrain_z_max(verlet3f_sim_t *sim, float z)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->body_cnt; ++i)
  {
    verlet3f_body_constrain_z_max(sim->bodies[i], z);
  }

  return true;
}

bool verlet3f_sim_constrain_z_min(verlet3f_sim_t *sim, float z)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->body_cnt; ++i)
  {
    verlet3f_body_constrain_z_min(sim->bodies[i], z);
  }

  return true;
}

bool verlet3f_sim_constrain_in_sphere(verlet3f_sim_t *sim,
                                      vec3f_t sphere_position,
                                      float sphere_radius)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->body_cnt; ++i)
  {
    verlet3f_body_constrain_in_sphere(sim->bodies[i], sphere_position,
                                      sphere_radius);
  }

  return true;
}

bool verlet3f_sim_constrain_out_sphere(verlet3f_sim_t *sim,
                                       vec3f_t sphere_position,
                                       float sphere_radius)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->body_cnt; ++i)
  {
    verlet3f_body_constrain_out_sphere(sim->bodies[i], sphere_position,
                                       sphere_radius);
  }

  return true;
}


bool verlet3f_sim_set_acceleration(verlet3f_sim_t *sim, vec3f_t acc)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->body_cnt; ++i)
  {
    verlet3f_body_set_acceleration(sim->bodies[i], acc);
  }

  return true;
}


bool verlet3f_sim_apply_force(verlet3f_sim_t *sim, vec3f_t force)
{
  {
    if (sim == NULL)
    {
      return false;
    }

    for (unsigned i = 0; i < sim->body_cnt; ++i)
    {
      verlet3f_body_apply_force(sim->bodies[i], force);
    }

    return true;
  }
}

bool verlet3f_sim_apply_force_in_area(verlet3f_sim_t *sim, float x_min,
                                      float x_max, float y_min, float y_max,
                                      float z_min, float z_max, vec3f_t force)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->body_cnt; ++i)
  {
    verlet3f_body_apply_force_in_area(sim->bodies[i], x_min, x_max, y_min,
                                      y_max, z_min, z_max, force);
  }

  return true;
}

bool verlet3f_sim_apply_resistance(verlet3f_sim_t *sim, float amount)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->body_cnt; ++i)
  {
    verlet3f_body_apply_resistance(sim->bodies[i], amount);
  }

  return true;
}
