#ifndef __VERLET3F_BODY__
#define __VERLET3F_BODY__

#include "verlet3f.h"

typedef struct
{
  enum
  {
    VERLET3F_BODY_CONSTRAIN_DIST,
    VERLET3F_BODY_CONSTRAIN_POS,
    VERLET3F_BODY_CONSTRAIN_INNER_COLLISIONS
  } type;

  union
  {
    struct
    {
      verlet3f_t *verlet0;
      verlet3f_t *verlet1;
      float dist;
    } dist;

    struct
    {
      verlet3f_t *verlet;
      vec3f_t pos;
    } pos;
  } data;

} verlet3f_body_constrain_t;

typedef struct
{
  verlet3f_t **verlets;
  unsigned verlet_cnt;
  unsigned verlet_cap;

  verlet3f_body_constrain_t *constrains;
  unsigned constrain_cnt;
  unsigned constrain_cap;
} verlet3f_body_t;

verlet3f_body_t *verlet3f_body_create();

bool verlet3f_body_delete(verlet3f_body_t **body);

bool verlet3f_body_add_verlet(verlet3f_body_t *body, verlet3f_t *verlet);

bool verlet3f_body_add_constrain(verlet3f_body_t *body,
                                 verlet3f_body_constrain_t constrain);

bool verlet3f_body_add_dist_constrain(verlet3f_body_t *body,
                                      unsigned verlet0_index,
                                      unsigned verlet1_index);

bool verlet3f_body_add_pos_constrain(verlet3f_body_t *body,
                                     unsigned verlet_intex, vec3f_t position);

bool verlet3f_body_add_all_dist_constrains(verlet3f_body_t *body);

bool verlet3f_body_resolve_constrains(verlet3f_body_t *body);

bool verlet3f_body_move(verlet3f_body_t *body, float dt);

bool verlet3f_body_set_acceleration(verlet3f_body_t *body, vec3f_t acc);

bool verlet3f_body_resolve_collisions(verlet3f_body_t *body0,
                                      verlet3f_body_t *body1);

bool verlet3f_body_constrain_x_max(verlet3f_body_t *body, float x);

bool verlet3f_body_constrain_x_min(verlet3f_body_t *body, float x);

bool verlet3f_body_constrain_y_max(verlet3f_body_t *body, float y);

bool verlet3f_body_constrain_y_min(verlet3f_body_t *body, float y);

bool verlet3f_body_constrain_z_max(verlet3f_body_t *body, float z);

bool verlet3f_body_constrain_z_min(verlet3f_body_t *body, float z);

bool verlet3f_body_constrain_in_sphere(verlet3f_body_t *body,
                                       vec3f_t sphere_position,
                                       float sphere_radius);

bool verlet3f_body_constrain_out_sphere(verlet3f_body_t *body,
                                        vec3f_t sphere_position,
                                        float sphere_radius);

bool verlet3f_body_apply_force(verlet3f_body_t *body, vec3f_t force);

bool verlet3f_body_apply_force_in_area(verlet3f_body_t *body, float x_min,
                                       float x_max, float y_min, float y_max,
                                       float z_min, float z_max, vec3f_t force);

bool verlet3f_body_apply_resistance(verlet3f_body_t *body, float amount);


bool verlet3f_body_bounding_box(verlet3f_body_t *body, vec3f_t *pos,
                                vec3f_t *size);

bool verlet3f_body_bounding_boxex_collide(vec3f_t pos0, vec3f_t size0,
                                          vec3f_t pos1, vec3f_t size1);

#endif
