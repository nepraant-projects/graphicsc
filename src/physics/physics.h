#ifndef __PHYSICS__
#define __PHYSICS__

#include "verlets/verlet3f.h"
#include "verlets/verlet3f_body.h"
#include "verlets/verlet3f_body_primitives.h"
#include "verlets/verlet3f_sim.h"

#endif
