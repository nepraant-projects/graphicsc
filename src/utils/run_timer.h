#ifndef __RUN_TIMER__
#define __RUN_TIMER__

#include <stdbool.h>
#include <time.h>

void run_timer_start(unsigned char id);

void run_timer_stop(unsigned char id);

void run_timer_set_name(unsigned char id, const char *name);

void run_timer_print_results();

#endif
