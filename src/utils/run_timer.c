#include "run_timer.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

static bool _run_timer_assigned[256] = {[0 ... 255] = false};
static unsigned _run_timer_runs[256];
static bool _run_timer_started[256] = {[0 ... 255] = false};
static clock_t _run_timer_start_time[256];
static double _run_timer_run_time_sum[256];
static double _run_timer_run_time_max[256];
static double _run_timer_run_time_min[256];
static bool _run_timer_name_assigned[256] = {[0 ... 255] = false};
static char _run_timer_name[16][256];

void _run_timer_init(unsigned char id)
{
  _run_timer_assigned[id] = true;
  _run_timer_runs[id] = 0;
  _run_timer_started[id] = false;
  _run_timer_run_time_sum[id] = 0.0;
  _run_timer_run_time_max[id] = 0.0;
  _run_timer_run_time_min[id] = INFINITY;
  _run_timer_name_assigned[id] = false;
}

void run_timer_start(unsigned char id)
{
  if (!_run_timer_assigned[id])
  {
    _run_timer_init(id);
  }

  _run_timer_started[id] = true;
  _run_timer_start_time[id] = clock();
}

void run_timer_stop(unsigned char id)
{
  if (!_run_timer_started[id])
  {
    return;
  }

  clock_t end_time = clock();
  double time_used =
      ((double)(end_time - _run_timer_start_time[id])) / CLOCKS_PER_SEC;

  if (_run_timer_run_time_max[id] < time_used)
  {
    _run_timer_run_time_max[id] = time_used;
  }

  if (_run_timer_run_time_min[id] > time_used)
  {
    _run_timer_run_time_min[id] = time_used;
  }
  _run_timer_runs[id]++;
  _run_timer_run_time_sum[id] += time_used;
}

void run_timer_set_name(unsigned char id, const char *name)
{
  _run_timer_name_assigned[id] = true;
  strncpy(_run_timer_name[id], name, 15);
}

void run_timer_print_results()
{
  printf("TIMER RESULTS:\n");
  for (int i = 0; i < 256; ++i)
  {
    if (!_run_timer_assigned[i])
    {
      continue;
    }
    printf("id: %3i, name: %15s, runs: %5i, avg: %1.4lf, max: %1.4lf, min: "
           "%1.4lf, sum: %1.4lf\n",
           i, _run_timer_name_assigned[i] ? _run_timer_name[i] : "UNNAMED",
           _run_timer_runs[i], _run_timer_run_time_sum[i] / _run_timer_runs[i],
           _run_timer_run_time_max[i], _run_timer_run_time_min[i],
           _run_timer_run_time_sum[i]);
  }
}
