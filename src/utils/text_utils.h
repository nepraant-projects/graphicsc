#ifndef __TEXT_UTILS__
#define __TEXT_UTILS__

long get_file_size(const char *filename);

char *get_file_text_allocated(const char *filename);

#endif
